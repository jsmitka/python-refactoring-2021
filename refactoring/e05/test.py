from decimal import Decimal
from pathlib import Path

from . import extract_function
from . import split_phase


BASE_DIR = Path(__file__).parent


def test_compute_revenue():
    orders = [
        extract_function.Order(
            items=[
                extract_function.OrderItem(unit_price=Decimal(10), quantity=5),
                extract_function.OrderItem(unit_price=Decimal(5), quantity=2),
            ],
            delivery_price=Decimal(15),
        ),
        extract_function.Order(
            items=[
                extract_function.OrderItem(unit_price=Decimal(500), quantity=1),
            ],
            delivery_price=Decimal(0),
            discount=Decimal(400),
        ),
    ]

    assert extract_function.compute_revenue(orders) == Decimal(175)


def test_load_orders_revenue():
    revenue = split_phase.load_orders_revenue(BASE_DIR / 'test_sample.xml')
    assert revenue == Decimal(175)
