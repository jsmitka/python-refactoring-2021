"""
Refactorings:
* Split Phase
"""

from decimal import Decimal
from pathlib import Path
from typing import Union
from xml.etree import ElementTree


FilePath = Union[str, Path]


def load_orders_revenue(file: FilePath) -> Decimal:
    total_revenue = Decimal(0)

    xml = ElementTree.parse(file)
    for order in xml.iterfind('./order'):
        item_price_el = order.find('./item_price')
        delivery_price_el = order.find('./delivery_price')
        discount_price_el = order.find('./discount')

        item_price = Decimal(item_price_el.text)
        delivery_price = Decimal(delivery_price_el.text)
        if discount_price_el is not None:
            discount_price = Decimal(discount_price_el.text)
        else:
            discount_price = Decimal(0)

        total_revenue += item_price + delivery_price - discount_price

    return total_revenue
