"""
Refactorings:
* Extract Function
"""

from dataclasses import dataclass
from decimal import Decimal
from typing import List, Optional


@dataclass()
class OrderItem:
    unit_price: Decimal
    quantity: int


@dataclass()
class Order:
    items: List[OrderItem]
    delivery_price: Decimal
    discount: Optional[Decimal] = None


def compute_revenue(orders: List[Order]) -> Decimal:
    total_revenue = Decimal(0)
    for order in orders:
        order_total = Decimal(0)
        for item in order.items:
            order_total += item.unit_price * item.quantity
        order_total += order.delivery_price
        if order.discount is not None:
            order_total -= order.discount
        total_revenue += order_total
    return total_revenue
