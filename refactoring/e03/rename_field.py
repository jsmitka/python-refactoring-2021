"""
Refactorings:
* Rename Field
* Rename Function
"""

from decimal import Decimal


class Order:
    def __init__(self, number: int, total_price: Decimal, dph: Decimal):
        """
        :param number: Order number.
        :param total_price: Total price of order.
        :param dph: DPH as % between 0 - 100, e.g. 20.
        """
        self.number = number
        self.total_price = total_price
        self.dph = dph

    def get_total_price_with_dph(self):
        tax_coefficient = Decimal(1) + self.dph / Decimal(100)
        return self.total_price * tax_coefficient
