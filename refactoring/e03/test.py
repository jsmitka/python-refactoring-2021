from decimal import Decimal

import pytest

from . import rename_field


@pytest.mark.parametrize(argnames=('order', 'expected'), argvalues=[
    (
        rename_field.Order(1, Decimal(100), Decimal(20)),
        Decimal(120),
    ),
    (
        rename_field.Order(1, Decimal(100), Decimal(0)),
        Decimal(100),
    ),
    (
        rename_field.Order(1, Decimal(100), Decimal(100)),
        Decimal(200),
    ),
])
def test_get_total_price(order: rename_field.Order, expected: Decimal):
    assert order.get_total_price_with_dph() == expected
