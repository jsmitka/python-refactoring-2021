from decimal import Decimal
from pathlib import Path

from . import encapsulate_record


BASE_DIR = Path(__file__).parent


def test_sum_orders():
    assert encapsulate_record.sum_orders(BASE_DIR / 'test_sample.xml') == Decimal(50)
