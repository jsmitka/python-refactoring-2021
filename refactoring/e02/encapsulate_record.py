"""
Refactorings:
* Encapsulate Record
"""

from decimal import Decimal
from pathlib import Path
from typing import Union

from xml.etree import ElementTree


def sum_orders(file: Union[str, Path]) -> Decimal:
    total = Decimal(0)
    for order in _load_orders(file):
        total += order['unit_price'] * order['quantity']
    return total


def _load_orders(file: Union[str, Path]):
    tree = ElementTree.parse(file)
    for order in tree.iterfind('./order'):
        unit_price_el = order.find('unit_price')
        quantity_el = order.find('quantity')
        yield {
            'unit_price': Decimal(unit_price_el.text),
            'quantity': int(quantity_el.text),
        }
