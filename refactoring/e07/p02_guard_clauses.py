"""
Refactorings:
* Replace Nested Conditionals with Guard Clauses - invert the conditions
"""

from decimal import Decimal


def get_capital_income(capital: Decimal, rate: Decimal, years: int) -> Decimal:
    result = Decimal(0)
    if capital > 0:
        if rate > 0 and years > 0:
            result = (capital * (1 + rate) ** years) - capital
    return result
