"""
Refactorings:
* Replace Nested Conditionals with Guard Clauses
* Consolidate Conditional Expression
"""

from xml.etree import ElementTree


def get_element_integer_value(element: ElementTree.Element) -> int:
    if not element.text:
        result = 0
    else:
        if element.text == '-':
            result = 0
        elif element.text.startswith('neg'):
            result = -1
        else:
            try:
                result = int(element.text)
            except ValueError:
                result = 0
    return result
