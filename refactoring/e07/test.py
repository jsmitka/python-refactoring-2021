from decimal import Decimal
from xml.etree import ElementTree

import pytest

from . import p01_guard_clauses as p01
from . import p02_guard_clauses as p02


@pytest.mark.parametrize(argnames=('xml', 'expected'), argvalues=[
    ('<em/>', 0),
    ('<em></em>', 0),
    ('<em>-</em>', 0),
    ('<em>neg</em>', -1),
    ('<em>42</em>', 42),
    ('<em>abc</em>', 0),
])
def test_get_element_integer_value(xml, expected):
    element = ElementTree.fromstring(xml)
    assert p01.get_element_integer_value(element) == expected


@pytest.mark.parametrize(argnames=('capital', 'rate', 'years', 'expected'), argvalues=[
    (Decimal(100), Decimal('0.1'), 1, Decimal(10)),
    (Decimal(100), Decimal('0.1'), 2, Decimal(21)),
    (Decimal(100), Decimal('0.1'), 0, Decimal(0)),
    (Decimal(100), Decimal(0), 1, Decimal(0)),
    (Decimal(0), Decimal('0.1'), 1, Decimal(0)),
    (Decimal(-5), Decimal('0.1'), 1, Decimal(0)),
])
def test_get_capital_income(capital, rate, years, expected):
    assert p02.get_capital_income(capital, rate, years) == expected
