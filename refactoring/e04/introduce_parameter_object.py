"""
Refactorings:
* Introduce Parameter Object
"""

import datetime
from dataclasses import dataclass
from decimal import Decimal
from typing import List, Tuple


@dataclass()
class Order:
    date: datetime.date
    total_price: Decimal


def get_order_count(orders: List[Order], start_date: datetime.date, end_date: datetime.date) -> int:
    result = 0
    for order in orders:
        if start_date <= order.date <= end_date:
            result += 1
    return result


def get_order_revenue(orders: List[Order], start_date: datetime.date, end_date: datetime.date) -> Decimal:
    result = Decimal(0)
    for order in orders:
        if start_date <= order.date <= end_date:
            result += order.total_price
    return result


def get_order_statistics(
        orders: List[Order], start_date: datetime.date, end_date: datetime.date
) -> Tuple[int, Decimal]:
    count = 0
    revenue = Decimal(0)
    for order in orders:
        if start_date <= order.date <= end_date:
            count += 1
            revenue += order.total_price
    return count, revenue
