import datetime
from decimal import Decimal

import pytest

from . import introduce_parameter_object


@pytest.fixture
def orders():
    return [
        introduce_parameter_object.Order(
            datetime.date(2020, 1, 1), Decimal(5),
        ),
        introduce_parameter_object.Order(
            datetime.date(2020, 1, 10), Decimal(20),
        ),
        introduce_parameter_object.Order(
            datetime.date(2020, 1, 15), Decimal(20),
        ),
        introduce_parameter_object.Order(
            datetime.date(2020, 2, 1), Decimal(2),
        ),
        introduce_parameter_object.Order(
            datetime.date(2020, 2, 15), Decimal(10),
        ),
    ]


def test_get_order_count(orders):
    assert introduce_parameter_object.get_order_count(
        orders, datetime.date(2020, 1, 10), datetime.date(2020, 2, 10),
    ) == 3


def test_get_order_revenue(orders):
    assert introduce_parameter_object.get_order_revenue(
        orders, datetime.date(2020, 1, 10), datetime.date(2020, 2, 10),
    ) == Decimal(42)


def test_get_order_statistics(orders):
    expected = (3, Decimal(42))

    assert introduce_parameter_object.get_order_statistics(
        orders, datetime.date(2020, 1, 10), datetime.date(2020, 2, 10),
    ) == expected
