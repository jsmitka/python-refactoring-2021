from decimal import Decimal

import pytest

from . import extract_variable


@pytest.mark.parametrize(argnames=('quantity', 'unit_price', 'expected'), argvalues=[
    (1, Decimal(20), Decimal(36),),
    (6, Decimal(20), Decimal(156),),
    (7, Decimal(20), Decimal(192),),
    (60, Decimal(20), Decimal(1560),),
])
def test_compute_order_total(quantity: int, unit_price: Decimal, expected: Decimal):
    assert extract_variable.compute_order_total(quantity, unit_price) == expected
