"""
Refactorings:
* Extract Variable
* Rename Variable
"""

import math
from dataclasses import dataclass
from decimal import Decimal


@dataclass()
class Order:
    quantity: int
    unit_price: Decimal


def compute_order_total(quantity, unit_price) -> Decimal:
    return (
        (
            (unit_price * quantity) + math.ceil(quantity / 6) * Decimal(10)
        ) * Decimal('1.2')
    )
