"""
Refactorings:
* Extract Variable (from Conditional)
"""

from dataclasses import dataclass
from decimal import Decimal
from enum import Enum


class CustomerType(Enum):
    END_USER = 'end_user'
    MERCHANT = 'merchant'


@dataclass()
class Cart:
    is_preorder: bool
    total_price: Decimal


@dataclass()
class Customer:
    is_registered: bool
    type_: CustomerType
    country: str


@dataclass()
class DeliveryMethod:
    price: Decimal
    free_delivery_threshold: Decimal


def compute_delivery_price(cart: Cart, customer: Customer, delivery_method: DeliveryMethod):
    if (
        customer.is_registered and customer.type_ == CustomerType.END_USER and customer.country == 'CZ'
        and not cart.is_preorder and cart.total_price > delivery_method.free_delivery_threshold
    ):
        return Decimal(0)
    return delivery_method.price

