from decimal import Decimal

import pytest

from . import p01_extract_variable as p01
from . import p02_decompose_conditional as p02


@pytest.fixture()
def delivery_method():
    return p01.DeliveryMethod(
        price=Decimal(20),
        free_delivery_threshold=Decimal(100),
    )


@pytest.mark.parametrize(argnames=('cart', 'customer', 'expected'), argvalues=[
    (
        p01.Cart(
            is_preorder=False,
            total_price=Decimal(200),
        ),
        p01.Customer(
            is_registered=True,
            type_=p01.CustomerType.END_USER,
            country='CZ',
        ),
        Decimal(0),
    ),
    (
        p01.Cart(
            is_preorder=True,
            total_price=Decimal(200),
        ),
        p01.Customer(
            is_registered=True,
            type_=p01.CustomerType.END_USER,
            country='CZ',
        ),
        Decimal(20),
    ),
    (
        p01.Cart(
            is_preorder=False,
            total_price=Decimal(10),
        ),
        p01.Customer(
            is_registered=True,
            type_=p01.CustomerType.END_USER,
            country='CZ',
        ),
        Decimal(20),
    ),
    (
        p01.Cart(
            is_preorder=False,
            total_price=Decimal(200),
        ),
        p01.Customer(
            is_registered=False,
            type_=p01.CustomerType.END_USER,
            country='CZ',
        ),
        Decimal(20),
    ),
    (
        p01.Cart(
            is_preorder=False,
            total_price=Decimal(200),
        ),
        p01.Customer(
            is_registered=True,
            type_=p01.CustomerType.MERCHANT,
            country='CZ',
        ),
        Decimal(20),
    ),
    (
        p01.Cart(
            is_preorder=False,
            total_price=Decimal(200),
        ),
        p01.Customer(
            is_registered=True,
            type_=p01.CustomerType.END_USER,
            country='SK',
        ),
        Decimal(20),
    ),
])
def test_compute_delivery_price(cart, customer, delivery_method, expected):
    assert p01.compute_delivery_price(cart, customer, delivery_method) == expected


class TestCart:
    @pytest.mark.parametrize(argnames=('cart', 'customer', 'expected'), argvalues=[
        (
            p02.Cart(
                total_price=Decimal(500),
                items_count=9,
            ),
            p02.Customer(
                type_=p02.CustomerType.END_USER,
                country='CZ',
            ),
            Decimal(30),
        ),
        (
            p02.Cart(
                total_price=Decimal(500),
                items_count=9,
            ),
            p02.Customer(
                type_=p02.CustomerType.END_USER,
                country='SK',
            ),
            Decimal(30),
        ),
        (
            p02.Cart(
                total_price=Decimal(1200),
                items_count=60,
            ),
            p02.Customer(
                type_=p02.CustomerType.END_USER,
                country='CZ',
            ),
            Decimal(0),
        ),
        (
            p02.Cart(
                total_price=Decimal(1200),
                items_count=90,
            ),
            p02.Customer(
                type_=p02.CustomerType.MERCHANT,
                country='CZ',
            ),
            Decimal(130),
        ),
        (
            p02.Cart(
                total_price=Decimal(400),
                items_count=60,
            ),
            p02.Customer(
                type_=p02.CustomerType.MERCHANT,
                country='SK',
            ),
            Decimal(150),
        ),
    ])
    def test_compute_delivery_price(self, cart: p02.Cart, customer: p02.Customer, expected: Decimal):
        assert cart.compute_delivery_price(customer) == expected
