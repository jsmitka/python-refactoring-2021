import math
from dataclasses import dataclass
from decimal import Decimal
from enum import Enum


class CustomerType(Enum):
    END_USER = 'end_user'
    MERCHANT = 'merchant'


@dataclass()
class Customer:
    type_: CustomerType
    country: str


MERCHANT_PKG_HANDLING = Decimal(30)
MERCHANT_PKG_SIZE = 60
MERCHANT_PKG_COST = Decimal(50)
END_USER_PKG_SIZE = 6
END_USER_PKG_COST = Decimal(15)


@dataclass()
class Cart:
    total_price: Decimal
    items_count: int

    def compute_delivery_price(self, customer: Customer) -> Decimal:
        if customer.type_ == CustomerType.MERCHANT and customer.country == 'CZ':
            return MERCHANT_PKG_HANDLING + math.ceil(
                self.items_count / MERCHANT_PKG_SIZE
            ) * MERCHANT_PKG_COST
        else:
            if self.total_price > Decimal(1000):
                return Decimal(0)
            else:
                return math.ceil(
                    self.items_count / END_USER_PKG_SIZE
                ) * END_USER_PKG_COST
