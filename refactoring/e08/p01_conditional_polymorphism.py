"""
Refactorings:
* Replace Conditional with Polymorphism
* Introduce Special Case / Null Object
"""
import math
from dataclasses import dataclass, field
from decimal import Decimal


@dataclass()
class DeliveryMethod:
    type_: str


@dataclass()
class Cart:
    total_price: Decimal
    weight: float
    """Package weight in kg."""
    discount: Decimal = field(default_factory=lambda: Decimal(0))

    def get_total_price(self, delivery_method: DeliveryMethod) -> Decimal:
        return self.total_price - self.discount + self.get_delivery_price(delivery_method)

    def get_delivery_price(self, delivery_method: DeliveryMethod) -> Decimal:
        if delivery_method.type_ == 'ppl':
            if self.total_price >= 1000:
                return Decimal(0)
            return Decimal(100)
        elif delivery_method.type_ == 'dpd':
            return math.ceil(self.weight / 20) * Decimal(50)
        elif delivery_method.type_ == 'cpost':
            if self.weight <= 10:
                return Decimal(50)
            else:
                return Decimal(120)
        else:
            raise ValueError('Unknown delivery type.')
