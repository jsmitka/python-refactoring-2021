import pytest
from decimal import Decimal

from . import p01_conditional_polymorphism as p01
from . import p02_special_case as p02


class TestCart:
    @pytest.mark.parametrize(argnames=('cart', 'delivery_method', 'expected'), argvalues=[
        (
            p01.Cart(total_price=Decimal(500), weight=25),
            p01.DeliveryMethod('ppl'),
            Decimal(100),
        ),
        (
            p01.Cart(total_price=Decimal(1000), weight=25),
            p01.DeliveryMethod('ppl'),
            Decimal(0),
        ),
        (
            p01.Cart(total_price=Decimal(1000), weight=15),
            p01.DeliveryMethod('dpd'),
            Decimal(50),
        ),
        (
            p01.Cart(total_price=Decimal(1000), weight=20),
            p01.DeliveryMethod('dpd'),
            Decimal(50),
        ),
        (
            p01.Cart(total_price=Decimal(1000), weight=21),
            p01.DeliveryMethod('dpd'),
            Decimal(100),
        ),
        (
            p01.Cart(total_price=Decimal(1000), weight=60),
            p01.DeliveryMethod('dpd'),
            Decimal(150),
        ),
        (
            p01.Cart(total_price=Decimal(1000), weight=10),
            p01.DeliveryMethod('cpost'),
            Decimal(50),
        ),
        (
            p01.Cart(total_price=Decimal(1000), weight=11),
            p01.DeliveryMethod('cpost'),
            Decimal(120),
        ),
    ])
    def test_get_delivery_price(self, cart: p01.Cart, delivery_method: p01.DeliveryMethod, expected: Decimal):
        assert cart.get_delivery_price(delivery_method) == expected


class TestCart2:
    def test_get_total_price_no_delivery(self):
        cart = p02.Cart(
            total_price=Decimal(100),
            weight=10.5,
            discount=Decimal(10),
        )
        assert cart.get_total_price(delivery_method=None) == Decimal(90)

    @pytest.mark.parametrize(argnames=('cart', 'delivery_method', 'expected'), argvalues=[
        (
                p02.Cart(total_price=Decimal(500), weight=25),
                p02.PPLDelivery(),
                Decimal(600),
        ),
        (
                p02.Cart(total_price=Decimal(1000), weight=25),
                p02.PPLDelivery(),
                Decimal(1000),
        ),
        (
                p02.Cart(total_price=Decimal(1000), weight=15),
                p02.DPDDelivery(),
                Decimal(1050),
        ),
        (
                p02.Cart(total_price=Decimal(1000), weight=20),
                p02.DPDDelivery(),
                Decimal(1050),
        ),
        (
                p02.Cart(total_price=Decimal(1000), weight=21),
                p02.DPDDelivery(),
                Decimal(1100),
        ),
        (
                p02.Cart(total_price=Decimal(1000), weight=60),
                p02.DPDDelivery(),
                Decimal(1150),
        ),
        (
                p02.Cart(total_price=Decimal(1000), weight=10),
                p02.CPostDelivery(),
                Decimal(1050),
        ),
        (
                p02.Cart(total_price=Decimal(1000), weight=11),
                p02.CPostDelivery(),
                Decimal(1120),
        ),
    ])
    def test_get_total_price(self, cart: p02.Cart, delivery_method: p02.DeliveryMethod, expected: Decimal):
        assert cart.get_total_price(delivery_method) == expected
