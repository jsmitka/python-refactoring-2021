"""
Refactorings:
* Replace Conditional with Polymorphism
* Introduce Special Case / Null Object
"""
import abc
import math
from dataclasses import dataclass, field
from decimal import Decimal
from typing import Optional


class DeliveryMethod(abc.ABC):
    @abc.abstractmethod
    def get_price(self, total_price: Decimal, weight: float) -> Decimal:
        pass


class PPLDelivery(DeliveryMethod):
    def get_price(self, total_price: Decimal, weight: float) -> Decimal:
        if total_price >= 1000:
            return Decimal(0)
        return Decimal(100)


class DPDDelivery(DeliveryMethod):
    def get_price(self, total_price: Decimal, weight: float) -> Decimal:
        return math.ceil(weight / 20) * Decimal(50)


class CPostDelivery(DeliveryMethod):
    def get_price(self, total_price: Decimal, weight: float) -> Decimal:
        if weight <= 10:
            return Decimal(50)
        else:
            return Decimal(120)


@dataclass()
class Cart:
    total_price: Decimal
    weight: float
    """Package weight in kg."""
    discount: Decimal = field(default_factory=lambda: Decimal(0))

    def get_total_price(self, delivery_method: Optional[DeliveryMethod] = None) -> Decimal:
        if delivery_method is not None:
            delivery_price = delivery_method.get_price(self.total_price, self.weight)
        else:
            delivery_price = 0
        return self.total_price - self.discount + delivery_price
